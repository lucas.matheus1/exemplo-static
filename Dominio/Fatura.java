package br.com.zup.AulaStatic.Dominio;

public class Fatura {

    //Atributos.
    private Consumidor pessoa;
    private Double valor;
    private String dataDeValidade;

    //Metódo Construtor.
    public Fatura(){}

    public Fatura(Consumidor pessoa, Double valor, String dataDeValidade) {
        this.pessoa = pessoa;
        this.valor = valor;
        this.dataDeValidade = dataDeValidade;
    }

    public Consumidor getPessoa() {
        return pessoa;
    }

    public void setPessoa(Consumidor pessoa) {
        this.pessoa = pessoa;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getDataDeValidade() {
        return dataDeValidade;
    }

    public void setDataDeValidade(String dataDeValidade) {
        this.dataDeValidade = dataDeValidade;
    }


    @Override
    public String toString() {

        StringBuilder retorno = new StringBuilder();
        retorno.append("\n------------------------");
        retorno.append("\nFatura " + this.pessoa);
        retorno.append("\nValor da Fatura R$" + this.valor);
        retorno.append("\nData da Valídade " + this.dataDeValidade);
        retorno.append("\n--------------------------");

        return retorno.toString();
    }
}
