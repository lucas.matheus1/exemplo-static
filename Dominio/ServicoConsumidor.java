package br.com.zup.AulaStatic.Dominio;

import java.util.ArrayList;
import java.util.List;

public class ServicoConsumidor {

    //Atributo List
    public static List<Consumidor> consumidores = new ArrayList<>();

    //Metódo static... Não preciso instanciar minha Classe Consumidor.
    public static void validarEmail(String email) throws Exception{



        if ((!email.contains("@"))) {

            throw new Exception("Erro o E-mail não possue o @");
        }


    }

    public static void addElementosNaLista(Consumidor consumidor){

        consumidores.add(consumidor);

    }

    public static void verificarEmailExiste(String email) throws Exception{
        for (Consumidor consumidor: consumidores) {
            if (consumidor.getEmail().equals(email)) {
                throw new Exception("Esse e-mail já existe!");
            }
        }
        
        
    }

    public static void mostrarLista(){
        System.out.println("Consumidor: ");
        for (Consumidor pecorrerLista: consumidores) {
            System.out.println(pecorrerLista);
        }

    }

    public static Consumidor cadastrarConsumidor(String nome, String cpf, String email)throws Exception{
        validarEmail(email);
        verificarEmailExiste(email);
        Consumidor obj_cosumidor = new Consumidor(nome, cpf , email);
        consumidores.add(obj_cosumidor);

        return obj_cosumidor;

    }

    public static Consumidor pesquisarConsumidorPorEmail(String email)throws Exception{

        validarEmail(email);
        for (Consumidor consumidor: consumidores) {
            if (consumidor.getEmail().equals(email)) {
                return consumidor;
            }

        }
        throw new Exception("Consumidor não cadastrado!");



    }

}
