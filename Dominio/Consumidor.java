package br.com.zup.AulaStatic.Dominio;

public class Consumidor {

    //Atributos...
    private String nome, cpf, email;


    //Metódo Construtor
    public Consumidor(){ }

    //Metódo Construtor
    public Consumidor(String nome, String cpf, String email) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
    }

    //Getters and Setters
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {

        StringBuilder retorno = new StringBuilder();
        retorno.append("\nNome: " + this.nome);
        retorno.append("\nCPF: " + this.cpf);
        retorno.append("\nEmail: " + this.email);

//        System.out.println("Consumidor: ");
//        System.out.println("Nome: " + this.nome);
//        System.out.println("CPF" + this.cpf);
//        System.out.println("Email: " + this.email);
        return retorno.toString();

    }
}
