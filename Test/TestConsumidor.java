package br.com.zup.AulaStatic.Test;

import br.com.zup.AulaStatic.Dominio.ServicoConsumidor;
import br.com.zup.AulaStatic.Dominio.ServicoFatura;

public class TestConsumidor {

    public static void main(String[] args) throws Exception {

//        Consumidor lucas = new Consumidor("Lucas", "3252364326", "lmatheus.tome@gmail.com");
//
//        System.out.println(lucas);
//
//        Fatura fatura = new Fatura(lucas, 700.0, "21/07/2021");
//
//        //Outra forma de instanciar...
//        Fatura fatura1 = new Fatura((new Consumidor("Matheus", "325236426", "matheus@123")), 700.0, "21/10/2021");
//
//        System.out.println(fatura);
//        System.out.println(fatura1);
//
//        ServicoConsumidor.addElementosNaLista((new Fatura(new Consumidor("Lucas", "31513532", "lmatheus@123"), 700.0, "03/09/2021"));
//
//        ServicoConsumidor.validarEmail(new Consumidor("Lucas", "3263264", "lmatheus"));


        try {
            ServicoConsumidor.cadastrarConsumidor("Andre", "43643634", "andre@andre");
            ServicoConsumidor.cadastrarConsumidor("lucas", "32523523", "lmatheus" );
            ServicoFatura.cadastrarFaturas("andre@andre", 20, "10/10/10");
            ServicoFatura.cadastrarFaturas("lmath@eus", 20, "10/10/10");

//            for (int i = 0; i < ServicoConsumidor.consumidores.size(); i++) {

            ServicoFatura.mostrarFatura();


//            }

        } catch (Exception excecao) {

            System.out.println(excecao.getMessage());

        }

    }



}
